#В строке найти и заменить одну подстроку на другую. Если одинаковых
#подстрок несколько, заменить все. Строка, значение которое надо заменить
#и значение, на которое надо заменить вводятся с клавиатуры. DON’T USE
#METHOD REPLACE

def replace(s, subs, sr):
    n = len(s)
    nsb = len(subs)
    sf = ""
    i = 0
    while i < n:
         if s[i:i+nsb] == subs:
              sf += sr
              i += nsb
         else:
              sf += s[i]
              i += 1
    return sf

s = input("insert string:")
subs = input("insert substring:")
sr = input("insert substring replacement:")

out = replace(s, subs, sr)
print(out)

#print(replace("olena prokopiv", "lena", "taniy"))