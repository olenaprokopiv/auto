#* Возведение в степень с помощью рекурсии

def power_rec(n,m):
    if m == 1:
       return n
    if m == 0:
       return 1
    if m > 1:
        result = n * power_rec(n,m - 1)
        return result
    if m < 0:
        return 1/power_rec(n,-m)

out = power_rec(3,4)
print(out)