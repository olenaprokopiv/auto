# Вводится ненормированная строка, у которой могут быть пробелы в начале,
# в конце и между словами более одного пробела. Привести ее к
# нормированному виду, т.е. удалить все пробелы в начале и конце, а между
# словами оставить только один пробел.

def one_spece(string):
    array = string.split(' ')
    str = ''
    for s in array:
        if not len(s) == 0:
            str += s
            str += ' '
    str = str.strip()
    return (str)

string = input("insert string:")

out = one_spece(string)
print(out)