#В строке, состоящей из слов, разделенных пробелом, найти самое длинное
#слово. Строка вводится с клавиатуры.

def longest_word(string):
    array = string.split()
    idLongestWord = 0
    for i in range(1,len(array)):
        if len(array[idLongestWord]) > len(array[idLongestWord]):
            idLongestWord = i
    return (array[idLongestWord])

#string = input("insert string:")

# out = longest_world(string)
# print(out)

def longest_word_str(string):
    array = string.split()
    LongestWord = ''
    for s in array:
        if len(s) > len(LongestWord):
            LongestWord = s
    return (LongestWord)

string = input("insert string:")

out = longest_word_str(string)
print(out)