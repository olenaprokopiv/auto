from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://www.amazon.com")

elem = driver.find_element(By.XPATH,"//input[@id='twotabsearchtextbox']")
elem.clear()
elem.send_keys('history book”')

elem = driver.find_element(By.XPATH,"//input[@type='submit'][@class='nav-input']")
elem.click()
sleep(2)

elem_links_list = driver.find_elements (By.XPATH, "//span[@data-component-type='s-product-image']")
print(elem_links_list)
print(type(elem_links_list))
