from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://www.amazon.com/gp/help/customer/display.html")

elem = driver.find_element(By.XPATH,"//input[@id='helpsearch']")
elem.clear()
elem.send_keys('cancel order')

elem = driver.find_element(By.XPATH,"//input[@class='a-button-input']")
elem.click()
sleep(2)

elem = driver.find_element(By.XPATH,"//div[@class='help-content']/h1")
print(elem.text)
assert elem.text == 'Cancel Items or Orders'
