from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://www.amazon.com/?tag")

elem = driver.find_element(By.XPATH,"//a[@id='nav-orders']")
elem.click()

sleep(2)

elem = driver.find_element(By.XPATH,"//h1[@class='a-spacing-small']")
assert elem.get_attribute("class") == 'a-spacing-small'
print(elem.get_attribute("class"))
