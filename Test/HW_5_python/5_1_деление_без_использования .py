#Division without the use of the division sign

def division(number_1, number_2):
    result = 0
    num = number_1
    while num >= number_2:
        num -= number_2
        result += 1
    rest = number_1 - result * number_2
    return result, rest

print(division(32, 5))