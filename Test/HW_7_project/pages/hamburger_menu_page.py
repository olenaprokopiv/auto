from selenium.webdriver.common.by import By
from HW_7_project.pages.base_page import Page
from selenium.webdriver.support import expected_conditions as EC


class HamburgermenuPage(Page):

    HAMBURGER_MENU_LOCATOR = (By.XPATH, "//div[@id='nav-belt']//i[@class='hm-icon nav-sprite']" )
    MUSIC_MENU_LOCATOR = (By.XPATH, "//ul[@class='hmenu  hmenu-visible']//li//a//div[contains(text(),'Amazon Music')]")
    MAIN_MENU_LOCATOR = (By.XPATH, "//div[@id='hmenu-content']//ul[@class='hmenu hmenu-visible hmenu-translateX']//li//a[@class='hmenu-item']")
    def open(self):
        self.open_page('https://www.amazon.com')

    def click_hamburger_menu(self):
        self.click(*self.HAMBURGER_MENU_LOCATOR)

    def click_music_menu_item(self):
        self.click(*self.MUSIC_MENU_LOCATOR)

    def get_menu_item_list(self, expected_items):
        elem_links_list = self.driver.find_elements(*self.MAIN_MENU_LOCATOR)
        n_expected_items = int(expected_items)
        print(len(elem_links_list))
        n_elem = len(elem_links_list)
        if n_elem > 0:
            assert n_elem == n_expected_items, f'expected {n_expected_items}, but got {n_elem}'
        else:
            print('Menu elements are not found')