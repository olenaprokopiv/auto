from selenium.webdriver.common.by import By
from HW_7_project.pages.base_page import Page

class CartPage(Page):

    CART_CLICK_LOCATOR = (By.XPATH,  "//a[@id='nav-cart']")
    EMPTY_CART_LOCATOR = (By.XPATH, "//div[@class='a-row sc-your-amazon-cart-is-empty']//h2")

    def open(self):
        self.open_page('https://www.amazon.com')

    def click_icon(self):
        self.click(*self.CART_CLICK_LOCATOR)

    def verify(self):
        result = True
        try:
           elem = self.find_element(*self.EMPTY_CART_LOCATOR)
           if elem.text == 'Your Amazon cart is empty':
               print('Your Amazon cart is empty')
           else:
               result = False
               print('Element does not contain Your Amazon cart is empty')
        except:
            result = False
            print('Your Amazon cart is empty element is not found')
        return result