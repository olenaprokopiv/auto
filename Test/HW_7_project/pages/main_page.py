from selenium.webdriver.common.by import By
from HW_7_project.pages.base_page import Page

class MainPage(Page):
    SEARCH_INPUT = (By.ID, 'twotabsearchtextbox')
    SEARCH_ICON = (By.XPATH, "//input[@class='nav-input'][@value = 'Go']")
    BUTTON_ORDERS_LOCATOR = (By.CSS_SELECTOR, '#nav-orders')

    def open(self):
        self.open_page('https://www.amazon.com')

    def click_orders(self):
        self.click(*self.BUTTON_ORDERS_LOCATOR)
