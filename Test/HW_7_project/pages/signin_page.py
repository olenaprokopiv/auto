from selenium.webdriver.common.by import By
from HW_7_project.pages.base_page import Page

class SigninPage(Page):

    SEARCH_INPUT = (By.ID, 'twotabsearchtextbox')
    SEARCH_ICON = (By.XPATH, "//input[@class='nav-input'][@value = 'Go']")
    BUTTON_ORDERS_LOCATOR = (By.CSS_SELECTOR, '#nav-orders')
    SIGNIN_LOCATOR = (By.XPATH, "//div[@id='authportal-main-section']//h1")

    def open(self):
        self.open_page('https://www.amazon.com')

    def click_orders(self):
        self.click(*self.BUTTON_ORDERS_LOCATOR)

    def verify(self):
        result = True
        try:
           elem = self.find_element(*self.SIGNIN_LOCATOR)
           if elem.text == 'Sign-In':
               print('Sign-In page is opened')
           else:
               result = False
               print('Element does not contain Sign-In')
        except:
            result = False
            print('Sign-In element is not found')
        return result


