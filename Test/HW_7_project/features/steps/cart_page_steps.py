from behave import given, when, then
from time import sleep


@when('Click on cart icon')
def Click_cart_icon(context):
    context.app.cart_page.click_icon()
    sleep(2)

@then('Verify Your Shopping Cart is empty')
def Verify_cart_is_empty_page(context):
    context.app.cart_page.verify()
    sleep(4)