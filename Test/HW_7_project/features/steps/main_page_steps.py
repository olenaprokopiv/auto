from behave import given, when, then
from time import sleep


@given('Open Amazon page')
def open_amazon(context):
    print("Open Amazon Page pt 1")
    context.app.main_page.open()

@when('Click Amazon Orders link')
def click_order_link(context):
    context.app.main_page.click_orders()

@then('Verify Sign In page is opened')
def verify_signin_page_opened(context):
     context.app.signin_page.verify()
     sleep(4)
