from behave import given, when, then
from time import sleep


@when('Click on hamburger menu')
def Click_hamburger_menu(context):
    context.app.hamburger_menu_page.click_hamburger_menu()
    sleep(2)

@when('Click on Amazon Music menu item')
def Click_music_menu_item(context):
    context.app.hamburger_menu_page.click_music_menu_item()
    sleep(2)

@then('Menu will have {expected_items} items')
def get_elem_links_list(context, expected_items):
    context.app.hamburger_menu_page.get_menu_item_list(expected_items)
    sleep(2)
