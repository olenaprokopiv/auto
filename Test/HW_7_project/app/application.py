from HW_7_project.pages.main_page import MainPage
from HW_7_project.pages.signin_page import SigninPage
from HW_7_project.pages.cart_page import CartPage
from HW_7_project.pages.hamburger_menu_page import HamburgermenuPage


class Application:
    def __init__(self, driver):
        self.driver = driver

        self.main_page = MainPage(self.driver)
        self.signin_page = SigninPage(self.driver)
        self.cart_page = CartPage(self.driver)
        self.hamburger_menu_page = HamburgermenuPage(self.driver)