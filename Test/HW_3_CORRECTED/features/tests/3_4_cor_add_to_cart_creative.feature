Feature: Test scenario for add an item and check the cart

  Scenario: User can add an item and check the cart
    Given Open Main Amazon page
    When Search item name
    And Click image of the first item
    And Click add to the cart
    When Add to the cart popup is shown
    Then Close Add to the cart popup
    Then Number of items in the cart more than zero
