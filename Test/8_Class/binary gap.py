#Найти наибольший пробел в двоичном числе,
# полученным из целочисленного положительного числа в десятичной системе,
# вводимого пользователем.

n = int(input('input number'))

def to_bin(n):
    bin_string = ''
    while n > 0:
        bin_string += str(n % 2)
        n = n // 2
    return bin_string[::-1]

def binary_gap(n):
    n = to_bin(n)
    max_gap = 0
    counter = 0
    bin_gap = False
    for item in n:
        if item == '1':
            bin_gap = False
            if max_gap < counter:
                max_gap = counter
        else:
             counter += 1
             bin_gap = True

     print(max_gap)  
    return (n)