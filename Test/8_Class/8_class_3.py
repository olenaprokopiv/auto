#Заполнить одномерный массив случайными числами.
# Найти и вывести на экран наибольший его элемент и индекс этого элемента

from random import randint

len_array = int(input('input the length of the array '))

array = []
for i in range(len_array):
    item = randint(-100, 100)
    array.append(item)
print(array)

def max_elem_and_index_elem(array):
    max_elem = array[0]
    max_index = 0
    for index in range(len(array)):
        i = array[index]
        if i > max_elem:
            max_elem = i
            max_index = index

    return max_elem, max_index
print( max_elem_and_index_elem(array))