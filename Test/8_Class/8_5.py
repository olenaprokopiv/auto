# Complete the function that takes two integers (a, b, where a < b)
# and return an array of all integers between the input parameters, including them.
# a = 1
# b = 4
# --> [1, 2, 3, 4]

def between(a,b):
    arr = []
    for i in range(a, b):
        arr.append(i)
    arr.append(b)
    return arr

out = between(2, 9)
print(out)
