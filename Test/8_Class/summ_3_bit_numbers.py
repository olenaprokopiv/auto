#Программа генерирует случайное трехзначное число и должна сложить цифры,
# из которых состоит это число. Например, если получено число 349,
# программа должна вывести на экран число 16, так как 3 + 4 + 9 = 16

from random import randint
n = randint (100, 999)

digits = int(input('bit number'))

print(n)

one = n % 10
ten = n // 10 % 10
hundred = n // 100

print(one + ten + hundred)


