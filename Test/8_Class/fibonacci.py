#Последовательность Фибоначчи определяется так:
#φ0 = 0, φ1 = 1, φn = φn−1 + φn−2.
#По данному числу n выведите на экран все числа ряда до n-го числа
#Фибоначчи φn.

# F0 = 0
# F1 = 1
# F2 = 1
# Fn = Fn-1 + Fn - 2

def fibonacci(n):
    fibonacci_1 = 1
    fibonacci_2 = 1
    if n == 0:
        print(0)
    if n > 0:
        print(fibonacci_1)
    if n > 1:
        print(fibonacci_2)
    i = 0
    while i < n - 2:
        fibonacci_sum = fibonacci_1 + fibonacci_2
        fibonacci_1, fibonacci_2 = fibonacci_2, fibonacci_sum
        i += 1
        print(fibonacci_sum)

fibonacci(8)


