#Задана матрица неотрицательных чисел.
# Посчитать сумму элементов в каждом столбце.
# Определить, какой столбец содержит максимальную сумму

def summ_and_max_col(matrix):
    ncol = len(matrix[0])
    nrow = len(matrix)
    max_summ = 0
    icol_max = 0
    for icol in range(ncol):
        summ = 0
        for irow in range(nrow):
            summ += matrix[irow][icol]
        if summ > max_summ:
            max_summ = summ
            icol_max = icol
    return  icol_max, max_summ

test_matrix = [[1, 7, 3], [1, 1, 1], [1, 0, 2], [1, 6, 9]]
out = summ_and_max_col(test_matrix)
print(out)
