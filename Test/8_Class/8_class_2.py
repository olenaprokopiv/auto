#Подсчитать количество положительных, отрицательных и равных нулю элементов массива

def pos_zero_neg(array):
    npos = 0
    nzero = 0
    nneg = 0
    for n in array:
        if n < 0:
            nneg +=1
        elif n > 0:
            npos += 1
        else:nzero += 1
    return npos, nzero, nneg
test_array = [5, -7, 6, -9, 1, 1, 0, 2]
print(test_array)
out = pos_zero_neg(test_array)
print(out)

