# Created by lena at 2/25/2020
Feature: Test for Amazon Search functionality

  Scenario: User can search for a product
    Given Open Amazon landing page
    When Search for dress
    Then Search results for "dress" is shown
