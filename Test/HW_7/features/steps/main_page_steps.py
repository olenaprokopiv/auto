from selenium.webdriver.common.by import By
from behave import given, when, then
from time import sleep

@given('Open Amazon landing page')
def open_amazon(context):
    context.app.main_page.open()

@when('Search for {product}')
def search_product(context, product):
    print('product = ', product)
    context.app.main_page.search_product(product)