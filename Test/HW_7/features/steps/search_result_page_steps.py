from behave import then
from selenium.webdriver.common.by import By

TOOL_BAR_TEXT_BOLD = (By.CSS_SELECTOR, 'h1 span.a-text-bold')

@then('Search results for {product} is shown')
def verify_header_result(context, product):
    result_text = context.driver.find_element(*TOOL_BAR_TEXT_BOLD).text
    context.app.results_page.verify_header_result(product)

