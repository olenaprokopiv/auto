from selenium.webdriver.common.by import By
from HW_7.pages.base_page import Page

class MainPage(Page):
    SEARCH_INPUT = (By.ID, 'twotabsearchtextbox')
    SEARCH_ICON = (By.XPATH, "//input[@class='nav-input'][@value = 'Go']")

    def open(self):
        self.open_page('https://www.amazon.com')

    def search_product(self, text: str):
        print('text = ', text)
        self.input(text, *self.SEARCH_INPUT)
        self.click(*self.SEARCH_ICON)