from selenium.webdriver.common.by import By
from HW_7.pages.base_page import Page

class ResultsPage(Page):
    TOOL_BAR_TEXT_BOLD = (By.CSS_SELECTOR, 'h1 span.a-text-bold')

    def verify_header_result(self, expected_text: str):
        self.verify_element_text(expected_text, *self.TOOL_BAR_TEXT_BOLD)
