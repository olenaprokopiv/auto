from selenium.webdriver.common.by import By
from behave import given, when, then
from time import sleep


COLORS_BTN_LOCATOR =(By.CSS_SELECTOR, '#variation_color_name ul[role=radiogroup] li')
COLOR_TITLE_LOCATOR = (By.CSS_SELECTOR, '#variation_color_name .selection' )
ADD_TO_CART_LOCATOR = (By.XPATH, "//input[@id='add-to-cart-button']")
POPUP_CLOSE_BTN = (By.XPATH, "//span[@id='a-autoid-1']")

@given('Open Amazon Alexa page')
def open_amazon_Alexa_page(context):
    context.driver.get('https://www.amazon.com/all-new-Echo/dp/B07NFTVP7P/ref=sr_1_2?crid=78QB55F2G77D&keywords=alexa&qid=1581427746&smid=ATVPDKIKX0DER&sprefix=alex%2Caps%2C143&sr=8-2')

@when('Get all Alexa colors')
def get_all_Alexa_colors(context):
    context.Alexa_colors = context.driver.find_elements(*COLORS_BTN_LOCATOR)

@then('Check every color has description')
def color_has_description(context):
    color_title = context.driver.find_element(*COLOR_TITLE_LOCATOR)
    for Alexa_color in context.Alexa_colors:
        Alexa_color.click()
        print(" color_title.text = %s  title = %s"  %(color_title.text,Alexa_color.get_attribute('title')))
        if( color_title.text == "Charcoal"): context.charcoal_btn = Alexa_color
        sleep(5)
        assert color_title.text in Alexa_color.get_attribute('title')


@then('Click Alexa_Charcoal_link')
def Click_Alexa_Charcoal_link(context):
    context.charcoal_btn.click()
    sleep(2)

@then('Add Alexa to the cart')
def Add_Alexa_to_cart(context):
       elem_button = context.driver.find_element(*ADD_TO_CART_LOCATOR)
       elem_button.click()
       #sleep(2)

@then('Close popup')
def close_popup(context):
    popup_close_buttons = context.driver.find_elements(*POPUP_CLOSE_BTN)
    print(popup_close_buttons)
    if len(popup_close_buttons) > 0:
        popup_close_buttons[0].click()
    #sleep(2)