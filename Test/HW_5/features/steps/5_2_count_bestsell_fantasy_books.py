from selenium.webdriver.common.by import By
from behave import given, when, then

SEARCH_BUTTON_LOCATOR = (By.XPATH, "//input[@class='nav-input'][@value = 'Go']")
BESTSELLER_LABEL_LOCATOR = (By.XPATH, "//span[@class ='a-badge-label']")

@given('Open Main Amazon page')
def open_main_amazon_page(context):
    print("Open Main Amazon page")
    context.driver.get("https://www.amazon.com")

@when('Send fantasy book')
def Send_fantasy_book(context):
    print("Send_fantasy_book")
    elem = context.driver.find_element(By.XPATH, "//input[@id='twotabsearchtextbox']")
    elem_button = context.driver.find_element(*SEARCH_BUTTON_LOCATOR)
    elem.clear()
    elem.send_keys('fantasy book')
    elem_button.click()

#@Count how much books would be shown in result list'
@then('Count bestseller books')
def get_elem_links_list(context):
    elem_links_list = context.driver.find_elements (*BESTSELLER_LABEL_LOCATOR)
    if len(elem_links_list) > 0:
        print(len(elem_links_list))
    else:
        print('not found')