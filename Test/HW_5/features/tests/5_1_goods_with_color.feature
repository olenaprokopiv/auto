# Make a test case similar to the example from class, use any good with color (or other options)

Feature: Alexa color

  Scenario: Check every Alexa color name
    Given Open Amazon Alexa page
    When Get all Alexa colors
    Then Check every color has description
    Then Click Alexa_Charcoal_link
    And  Add Alexa to the cart
    Then Close popup