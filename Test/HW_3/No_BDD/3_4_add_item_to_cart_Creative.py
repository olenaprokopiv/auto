from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://www.amazon.com/")

elem = driver.find_element(By.XPATH,"//input[@id='twotabsearchtextbox']")
elem_button = driver.find_element(By.XPATH,"//input[@class='nav-input'][@value='Go']")

elem.clear()
elem.send_keys('shark robot vacuum')

elem_button.click()
sleep(2)

elem_img = driver.find_element(By.XPATH,"//span[@cel_widget_id='SEARCH_RESULTS-SEARCH_RESULTS']//img[@data-image-index='0']")
elem_img.click()
sleep(2)

elem_button = driver.find_element(By.XPATH,"//input[@id='add-to-cart-button']")
elem_button.click()
sleep(2)

elem_button = driver.find_element(By.XPATH,"//button[@id='attachSiAddCoverage-announce']")
elem_button.click()
sleep(2)

elem = driver.find_element(By.XPATH,"//span[@id='attach-accessory-cart-total-string']")
print(elem.text)

str = elem.text
ib=str.find("(")
ii=str.find("i")
strnum=str[ib+1:ii-1]
print(strnum)
n=int(strnum)

assert n > 0






