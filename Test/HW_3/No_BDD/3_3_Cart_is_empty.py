from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://www.amazon.com")
#Click Cart
elem = driver.find_element(By.CSS_SELECTOR,"#nav-cart")
elem.click()

elem = driver.find_element(By.CSS_SELECTOR,".sc-empty-cart-header")
print(elem.text)
assert elem.text == "Your Shopping Cart is empty."