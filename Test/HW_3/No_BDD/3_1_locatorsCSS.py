from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By

chrome_options = webdriver.ChromeOptions()
chrome_options.add_argument("--incognito")
driver = webdriver.Chrome(chrome_options=chrome_options)

driver.get("https://www.amazon.com/sign-in")
sleep(2)

elem = driver.find_element(By.CSS_SELECTOR,"#createAccountSubmit")
elem.click()
#Amazon
elem = driver.find_element(By.CSS_SELECTOR,".a-link-nav-icon")
assert elem.get_attribute("class") == 'a-link-nav-icon'
#Create account
elem = driver.find_element(By.CSS_SELECTOR,"h1.a-spacing-small")
assert elem.text == 'Create account'
#Your name
elem = driver.find_element(By.CSS_SELECTOR,"#ap_customer_name")
assert elem.get_attribute("name") == 'customerName'
#Email
elem = driver.find_element(By.CSS_SELECTOR,"#ap_email")
assert elem.get_attribute("name") == 'email'
#Password
elem = driver.find_element(By.CSS_SELECTOR,"#ap_password")
assert elem.get_attribute("name") == 'password'
#Re-enter password
elem = driver.find_element(By.CSS_SELECTOR,"#ap_password_check")
assert elem.get_attribute("name") == 'passwordCheck'
#Create your Amazon account
elem = driver.find_element(By.CSS_SELECTOR,".a-button-input")
assert elem.get_attribute("type") == 'submit'
#Conditions of Use
#Privacy Notice
#Sign in
elem = driver.find_element(By.CSS_SELECTOR,".a-link-emphasis")

