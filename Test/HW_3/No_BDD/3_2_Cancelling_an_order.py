from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from behave import given, when, then

SEARCH_INPUT_LOCATOR = (By.XPATH,"//input[@id='helpsearch']")
SEARCH_BUTTON_LOCATOR = (By.XPATH,"//input[@class='a-button-input']")
SEARCH_CANCEL_ORDER = (By.XPATH,"//div[@class='help-content']/h1")
@given('Open Amazon page')

def open_amazon_page(context):
    print("Open Amazon main page")
    context.driver = webdriver.Chrome()
    context.driver.get('https://www.amazon.com/gp/help/customer/display.html')

@when('Input Cancel Order into search field')
def Search_input_fill(context):
    print("Search_input_fill")
    amazon_search_input = context.driver.find_element(*SEARCH_INPUT_LOCATOR)
    amazon_search_input.clear()
    amazon_search_input.send_keys("Cancel Order")

@when('Click on Go')
def Click_search_button(context):
    print("Click_search_button")
    amazon_search_button = context.driver.find_element(*SEARCH_BUTTON_LOCATOR)
    amazon_search_button.click()
    sleep(2)

@then('Results for Cancel Order are shown')
def Find_cancel_order_on_page(context):
    print("Find_text_on_page")
    elem = context.driver.find_element(*SEARCH_CANCEL_ORDER)
    assert elem.text == 'Cancel Items or Orders'