Feature: Test scenario for Cancel Order

  Scenario: User can cancel order
    Given Open Amazon page
    When Input Cancel Order into search field
    And Click on Go
    Then Results for Cancel Order are shown
