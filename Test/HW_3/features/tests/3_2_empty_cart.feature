Feature: Test scenario for empty cart

  Scenario: User can check if cart is empty
    Given Open Main Amazon page
    When Click cart button
    Then Empty cart is shown
