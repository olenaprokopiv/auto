from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from behave import given, when, then

CART_CLICK_LOCATOR = (By.CSS_SELECTOR,"#nav-cart")
EMPTY_CART_LOCATOR = (".sc-empty-cart-header")

@given('Open Main Amazon page')
def open_main_amazon_page(context):
    print("Open Amazon main page")
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")
    context.driver = webdriver.Chrome(chrome_options=chrome_options)
    context.driver.get("https://www.amazon.com")

@when('Click cart button')
def Click_cart_button(context):
    print("Click cart button")
    amazon_cart_click = context.driver.find_element(*CART_CLICK_LOCATOR)
    amazon_cart_click.clear()
    amazon_cart_click.send_keys("Click cart button")
    sleep(2)

@then('Empty cart is shown')
def Find_cancel_order_on_page(context):
    print("Empty cart is shown")
    elem = context.driver.find_element(*EMPTY_CART_LOCATOR)
    assert elem.text == 'Your Shopping Cart is empty.'
