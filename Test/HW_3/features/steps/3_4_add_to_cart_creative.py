from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from behave import given, when, then

@given('Open Main Amazon page2')
def open_main_amazon_page(context):
    print("Open Amazon main page")
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--incognito")
    context.driver = webdriver.Chrome(chrome_options=chrome_options)
    context.driver.get("https://www.amazon.com")


@when('Search item name')
def Search_item_name(context):
    print("Search item name")
    elem = context.driver.find_element(By.XPATH, "//input[@id='twotabsearchtextbox']")
    elem_button = context.driver.find_element(By.XPATH, "//input[@class='nav-input'][@value='Go']")
    elem.clear()
    elem.send_keys('irobot Roomba 675')
    elem_button.click()
    sleep(2)


@when('Click image of the first item')
def Click_first_item(context):
    print("Click image of the first item")
    elem_img = context.driver.find_element(By.XPATH,"//span[@cel_widget_id='SEARCH_RESULTS-SEARCH_RESULTS']//img[@data-image-index='0']")
    elem_img.click()
    sleep(2)

@when('Click add to the cart')
def Click_add(context):
    print("Click add to the cart")
    elem_button = context.driver.find_element(By.XPATH, "//input[@id='add-to-cart-button']")
    elem_button.click()
    sleep(4)
    print("Click add coverage")
    try:
        elem_button = context.driver.find_element(By.XPATH,"//span[@id='siNoCoverage']//button[@class ='a-button-text']")
        elem_button.click()
    except:
        print("Coverage not found")
    sleep(2)

@then('Number of items in the cart more than zero')
def Number_of_items(context):
    print("Number of items in the cart more than zero")

    elem = context.driver.find_element(By.XPATH, "//div[@id='hlb-subcart']/div/span/span")
    print(elem.text)
    str = elem.text
    ib = str.find("(")
    ii = str.find("i")
    strnum = str[ib + 1:ii - 1]
    print(strnum)
    n = int(strnum)

    assert n > 0