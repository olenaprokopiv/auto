#Найти максимальный элемент среди минимальных элементов столбцов матрицы

def max_elem_col(matrix):
    nrow = len(matrix)
    ncol = len(matrix[0])
    min_array = []
    for icol in range(ncol):
       min_elem = matrix[0][icol]
       for irow in range(nrow):
           if matrix[irow][icol] < min_elem:
               min_elem = matrix[irow][icol]
       min_array.append(min_elem)
    print(min_array)
    max_elem = min_array[0]
    for elem in min_array:
        if elem > min_array[0]:
            max_elem = elem
    return max_elem

test_matrix = [[5, 7, 3], [9, 1, 1], [4, 0, 2], [5, 6, 9]]
out = max_elem_col(test_matrix)
print(out)