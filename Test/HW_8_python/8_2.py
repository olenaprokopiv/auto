#2.	В одномерном массиве целых чисел определить два наименьших элемента.
# Они могут быть как равны между собой (оба являться минимальными), так и различаться

def min_element(array):
    minimum = []
    for i in array:
        if len(minimum) < 2:
          minimum.append(i)
          continue
        if i < minimum[0]:
            minimum[0] = i
            continue
        if i < minimum[1]:
            minimum[1] = i
    return minimum

test_array = [5, 7, 6, 9, 1, 1, 0, 2]
out = min_element(test_array)
print(out)





