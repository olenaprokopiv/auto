#3.	Найти максимальный элемент среди минимальных элементов строк матрицы.

def maximum_element(matrix):
    min_arr = []
    for row in matrix:
        min_elem = row[0]
        for elem in row:
            if elem < min_elem:
               min_elem = elem
        min_arr.append(min_elem)
    print(min_arr)
    max_elem = min_arr[0]
    for elem in min_arr:
        if elem > max_elem:
            max_elem = elem
    return max_elem

test_matrix = [[5, 7, 3], [9, 1, 1], [4, 0, 2]]
out = maximum_element(test_matrix)
print(out)