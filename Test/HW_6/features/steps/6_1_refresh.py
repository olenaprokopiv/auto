from selenium.webdriver.common.by import By
from behave import given, when, then
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

ITEM_LINK_LOCATOR = (By.XPATH, "//div[@id='desktop-7']//div[@class='a-section a-spacing-none feed-carousel']//a[@class='a-link-normal']")
ADD_TO_CART_LOCATOR = (By.XPATH, "//input[@id='add-to-cart-button']")
POPUP_CLOSE_BTN = (By.XPATH, "//span[@id='attachSiNoCoverage']//button[@id='attachSiNoCoverage-announce']")


@given('Open Amazon page')
def open_amazon_page(context):
    context.driver.get('https://www.amazon.com')

@when('Store original window')
def store_original_window(context):
    context.init_window = context.driver.current_window_handle

@when('Click to item link')
def click_to_item_link(context):
    item_links = context.driver.find_elements(*ITEM_LINK_LOCATOR)
    print(item_links)
    item_links[0].click()
    context.all_windows = context.driver.window_handles
    sleep(2)

@when('Click add to the cart')
def Click_add(context):
    print("Click add to the cart")
    elem_button = context.driver.find_element(*ADD_TO_CART_LOCATOR)
    sleep(2)
    elem_button.click()
    sleep(2)

@then('Close popup')
def close_popup(context):
    popup_close_buttons = context.driver.find_elements(*POPUP_CLOSE_BTN)
    print(popup_close_buttons)
    sleep(2)
    if len(popup_close_buttons) > 0:
        popup_close_buttons[0].click()
    sleep(2)

@then('Switch back to original')
def go_back_to_origin_window(context):
     print(context.init_window)
     #context.driver.close()
     context.driver.switch_to.window(context.init_window)
     sleep(2)

@then('To refresh the page')
def refresh_the_page(context):
    context.driver.refresh()
    sleep(2)

@then('To verify cart has 1 item')
def cart_has_1_item(context):
    print("To verify cart has 1 item")

    elem = context.driver.find_element(By.XPATH, "//div[@id='hlb-subcart']/div/span/span")
    print(elem.text)
    str = elem.text
    ib = str.find("(")
    ii = str.find("i")
    strnum = str[ib + 1:ii - 1]
    print(strnum)
    n = int(strnum)

    assert n > 0


