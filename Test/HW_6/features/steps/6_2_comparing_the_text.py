from selenium.webdriver.common.by import By
from behave import given, when, then
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

BEST_SELLERS_MAIN_LOCATOR = (By.XPATH, "//div[@class='nav-fill']//a[text()='Best Sellers']")
BEST_SELLERS_LOCATOR = (By.XPATH, "//div[@id='zg_tabs']//a[text()='Best Sellers']")
NEW_RELEASES_LOCATOR = (By.XPATH, "//div[@id='zg_tabs']//a[text()='New Releases']")
MOVERS_SHAKERS_LOCATOR = (By.XPATH, "//div[@id='zg_tabs']//a[text()='Movers & Shakers']")
MOST_WISHED_LOCATOR = (By.XPATH, "//div[@id='zg_tabs']//a[text()='Most Wished For']" )
GIFT_IDEAS_LOCATOR = (By.XPATH, "//div[@id='zg_tabs']//a[text()='Gift Ideas']" )

@given('Open Amazon main page')
def open_amazon_page(context):
    context.driver.get('https://www.amazon.com')

@when('Click on Best Sellers link')
def click_Best_Sellers_link(context):
    print("Click on Best Sellers link")
    elem_button = context.driver.find_element(*BEST_SELLERS_MAIN_LOCATOR)
    elem_button.click()
    sleep(2)

@then('Click on each tab and check titles')
def click_on_tab_and_check(context):
    tab_names= ['Best Sellers','New Releases','Movers & Shakers','Most Wished For','Gift Ideas']
    tab_locs = []
    tab_locs.append(BEST_SELLERS_LOCATOR)
    tab_locs.append(NEW_RELEASES_LOCATOR)
    tab_locs.append(MOVERS_SHAKERS_LOCATOR)
    tab_locs.append(MOST_WISHED_LOCATOR)
    tab_locs.append(GIFT_IDEAS_LOCATOR)
    for loc in tab_locs:
        tab_elem = ""
        try:
            tab_elem = context.driver.find_element(*loc)
            tab_text = tab_elem.text
            tab_elem.click()
            sleep(2)
            title_elem = context.driver.find_element(By.XPATH, "//div[@id='zg_banner_text_wrapper']")
            title_text = title_elem.text
            if tab_text in title_text:
                print(tab_text, '  found')
            else:
                print(tab_text, '  not found')

        except:
             print(tab_elem, "did not work")
        sleep(3)
    exit(0)


