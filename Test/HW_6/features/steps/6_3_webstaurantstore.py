from selenium.webdriver.common.by import By
from behave import given, when, then
from selenium.webdriver.support import expected_conditions as EC
from time import sleep

INPUT_LOCATOR = (By.XPATH, "//input[@type='text'][@id='searchval']")
SEARCH_BUTTON_LOCATOR = (By.XPATH, "//div[@class='banner-search-group']//button[@type='submit'][@value='Search']")
PRODUCT_DETAILS = (By.XPATH, "//div[@id='product_listing']//div[@class='details']//a[@class='description']")
CART_CLICK_LOCATOR = (By.XPATH, "//div[@class='pull-right global-menu--right']//a[@class='menu-btn']")
EMPTY_CART_LOCATOR = (By.XPATH, "//div[@class='cartItemsHeader toolbar clears']//a[@class='emptyCartButton btn btn-mini btn-ui pull-right']")

@given('Open webstaurantstor page')
def open_amazon_page(context):
    context.driver.get('https://www.webstaurantstore.com/')

@when('Send stainless work table')
def Send_stainless_work_table(context):
    print("Send stainless work table")
    elem = context.driver.find_element(*INPUT_LOCATOR)
    elem_button = context.driver.find_element(*SEARCH_BUTTON_LOCATOR)
    elem.clear()
    elem.send_keys('stainless work table')
    elem_button.click()

@then('Count stainless work table {expected_items} Table its title')
def get_elem_links_list(context, expected_items):
    context.elem_links_list = context.driver.find_elements (*PRODUCT_DETAILS)
    print('expected number of items = ', expected_items)
    print('actual number of items = ', len(context.elem_links_list))


@when('Check every item has the word Table its title')
def check_word_table(context):
    count_found = 0
    count_not_found = 0
    for elem in context.elem_links_list:
        if 'Table' in elem.text:
            count_found += 1
        else:
            count_not_found += 1
    print('Number of Table found = ',count_found)
    print('Number of Table not found = ',count_not_found)

@then('Add the last of found items to Cart')
def Click_add(context):
    print("Click add to cart")
    elem_buttons = context.driver.find_elements(By.XPATH, "//div[@id='product_listing']//input[@type='submit']")
    elem_buttons[-1].click()
    sleep(10)

@when('Click cart button')
def Click_cart_button(context):
    elem_button = context.driver.find_element(*CART_CLICK_LOCATOR)
    elem_button.click()
    sleep(4)

@then('Empty cart is shown')
def Find_cancel_order_on_page(context):
    elem = context.driver.find_element(*EMPTY_CART_LOCATOR)
    print('Your Shopping Cart is empty = ',elem.text)
    sleep(5)


