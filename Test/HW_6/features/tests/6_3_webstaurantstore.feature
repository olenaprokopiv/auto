# Created by lena at 2/20/2020
Feature: Test scenario for Every product item has the word 'Table' its title
    Add the last of found items to Cart
    Empty Cart

  Scenario: Every product item has the word 'Table' its title
    Add the last of found items to Cart
    Empty Cart
Given Open webstaurantstor page
When  Send stainless work table
Then Count stainless work table 60 Table its title
When Check every item has the word Table its title
Then Add the last of found items to Cart
When Click cart button
Then Empty cart is shown