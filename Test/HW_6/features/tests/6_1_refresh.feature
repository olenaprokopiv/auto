# Created by lena at 2/18/2020
Feature: Test scenario for refresh the page

   Scenario: User can add item to the cart then to refresh the page
Given Open Amazon page
When Store original window
And Click to item link
When Click add to the cart
Then Close popup
Then Switch back to original
Then To refresh the page
Then To verify cart has 1 item
