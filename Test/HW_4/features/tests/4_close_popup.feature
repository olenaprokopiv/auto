# Created by lena at 2/4/2020
Feature: Close popup

  Scenario: If we have popup, we will close it
    Given Open Heritage site
    When See popup
    Then Close popup
