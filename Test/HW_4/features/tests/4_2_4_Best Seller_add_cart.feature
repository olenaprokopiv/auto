# Created by lena at 2/4/2020
#If last book has “Best Seller” label, will add it in the cart (go to page of book)

Feature: Test scenario for count historic books

  Scenario: User can count historic books
    Given Open Main Amazon page2
    When Search history book4
    And Get last book link
    Then Click Book link if bestseller
    And  Add Book to the cart
