from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from behave import given, when, then

SEARCH_BUTTON_LOCATOR = (By.XPATH, "//input[@class='nav-input'][@value = 'Go']")

@given('Open Main Amazon page prime')
def open_main_amazon_page(context):
    print("Open Amazon main page prime")
    context.driver.get("https://www.amazon.com/amazonprime")

@when('find section Shopping')
def find_section_Shopping(context):
    print("find section Shopping")
    elem = context.driver.find_element(By.XPATH, "//div[@class = 'a-section a-padding-base']")

@then('section Shopping will have {expected_items} items')
def get_elem_links_list(context, expected_items):
    elem_links_list = context.driver.find_elements (By.XPATH, "//div[@class = 'a-section a-padding-base']")
    expected_items = int(expected_items)
    print(len(elem_links_list))
    if len(elem_links_list) > 0:
        assert len(elem_links_list) == expected_items, f'expected {expected_items}, but got {elem_links_list}'
    else:
        print('not found')



