from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from behave import given, when, then

SEARCH_BUTTON_LOCATOR = (By.XPATH, "//input[@value='Go']")
LABEL_BEST_SELLER = (By.XPATH, "")

@given('Open Main Amazon page2')
def open_main_amazon_page(context):
    print("Open Amazon main page")
    context.driver.get("https://www.amazon.com")

@when('Search history book4')
def Search_history_book(context):
    print("Search history book")
    elem = context.driver.find_element(By.XPATH, "//input[@id='twotabsearchtextbox']")
    elem_button = context.driver.find_element(*SEARCH_BUTTON_LOCATOR)
    elem.clear()
    elem.send_keys('history book')
    elem_button.click()

@when('Get last book link')
def get_last_book_link(context):
    elem_links_list = context.driver.find_elements(By.XPATH, "//span[@data-component-type='s-product-image']/a")
    n = len(elem_links_list)
    print(n)
    context.last_book = elem_links_list[n-1]
    elem = context.last_book
    print("href =", elem.get_attribute("href"))
    elem_parent = elem.find_element_by_xpath('./../../../../../..')
    print(elem_parent.get_attribute("class"))
    elem_rows = elem_parent.find_elements(By.XPATH, "./div[@class='sg-row']")
    print("num rows = ", len(elem_rows))
    context.is_bestseller = False
    try:
        context.bestseller_link = elem_rows[0].find_element(By.XPATH, ".//a[@class='a-link-normal']")
        context.is_bestseller = True
    except:
        print("bestseller link not found")

@then('Click Book link if bestseller')
def Click_book_link(context):
    print("Click image of the Best Seller item")
    if(not context.is_bestseller):
        context.last_book.click()

@then('Add Book to the cart')
def Add_book_to_cart(context):
    print("Add Book to the cart")
    if( not context.is_bestseller ):
       elem_button = context.driver.find_element(By.XPATH, "//input[@id='add-to-cart-button']")
       elem_button.click()
