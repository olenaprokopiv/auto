from time import sleep
from selenium import webdriver
from selenium.webdriver.common.by import By
from behave import given, when, then

SEARCH_BUTTON_LOCATOR = (By.XPATH, "//input[@class='nav-input'][@value = 'Go']")

@given('Open Main Amazon page4_2')
def open_main_amazon_page(context):
    print("Open Amazon main page")
    context.driver.get("https://www.amazon.com")

@when('Send history book4_2')
def Send_history_book(context):
    print("Send_history_book")
    elem = context.driver.find_element(By.XPATH, "//input[@id='twotabsearchtextbox']")
    elem_button = context.driver.find_element(*SEARCH_BUTTON_LOCATOR)
    elem.clear()
    elem.send_keys('history book')
    elem_button.click()

#@Count how much books would be shown in result list'
@then('Menu will have {expected_items} items4_2')
def get_elem_links_list(context, expected_items):
    elem_links_list = context.driver.find_elements (By.XPATH, "//span[@data-component-type='s-product-image']")
    expected_items = int(expected_items)
    print(len(elem_links_list))
    if len(elem_links_list) > 0:
        assert len(elem_links_list) == expected_items, f'expected {expected_items}, but got {elem_links_list}'
    else:
        print('not found')